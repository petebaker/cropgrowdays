## File: Makefile.runR
## 
## What: Template typical rules for R files but adds extension R_OUT_EXT
## 
##   - take a copy and modify as necessary
##   - ideally put modified copy in a directory you access often or write
##     a script to grab a copy whenever you need

RMARKDOWN_GITHUB_OPTS = 'github_document'
RMARKDOWN_GITHUB_EXTRAS =
DESCRIPTION=DESCRIPTION
FRAGMENTS=man/fragments/intro.Rmd man/fragments/boonah_description.Rmd man/fragments/crop_data_description.Rmd man/fragments/day_of_year_calcs.Rmd man/fragments/agro_met_calcs.Rmd
HEADER_README=man/fragments/readme_latex_header.Rmd
BIB=man/fragments/cropgrowdays.bib

.PHONY: all
all: README.md README.pdf

## for testing but not updating README.pdf (so git doesn't need to get updated)  -----
test: test.pdf test.md
test.pdf: README.Rmd ${DESCRIPTION} ${FRAGMENTS} ${BIB} ${HEADER_README}
	${RSCRIPT} ${RSCRIPT_OPTS} -e "library(rmarkdown);render('README.Rmd', output_file = 'test.pdf', ${RMARKDOWN_PDF_OPTS} ${RMARKDOWN_PDF_EXTRAS})"
test.md: README.Rmd  ${DESCRIPTION} ${FRAGMENTS} ${BIB}
	${RSCRIPT} ${RSCRIPT_OPTS} -e "library(rmarkdown);render('README.Rmd', output_file = 'test.md', ${RMARKDOWN_GITHUB_OPTS} ${RMARKDOWN_GITHUB_EXTRAS})"
## END: for testing README.Rmd (so git doesn't need to get updated)  ----------------

## .md from .Rmd  - github doc
%.md: %.Rmd  ${DESCRIPTION} ${FRAGMENTS} ${BIB}
	${RSCRIPT} ${RSCRIPT_OPTS} -e "library(rmarkdown);render('${@:.md=.Rmd}', ${RMARKDOWN_GITHUB_OPTS} ${RMARKDOWN_GITHUB_EXTRAS})"
%.md: %.rmd ${DESCRIPTION}  ${FRAGMENTS} ${BIB}
	${RSCRIPT} ${RSCRIPT_OPTS} -e "library(rmarkdown);render('${@:.md=.rmd}', ${RMARKDOWN_GITHUB_OPTS} ${RMARKDOWN_GITHUB_EXTRAS})"

README.pdf: README.Rmd  ${DESCRIPTION} ${FRAGMENTS} ${BIB}

##R_OUT_EXT = Rout
R_OUT_EXT = pdf
##R_OUT_EXT = html
##R_OUT_EXT = docx
##R_OUT_EXT = odt
##R_OUT_EXT = rtf

RMD_OUT_EXT = docx

DATAFILE=

## readme.md
README.md: README.Rmd

## unclude pattern rules
include ~/lib/r-rules.mk
