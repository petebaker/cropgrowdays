This update is a minor fix for the breaking change in 'Roxygen' where
@docType package no longer automatically adds a -package alias, as
advised by email from Kurt Hornik <Kurt.Hornik@wu.ac.at> on Sat, 19
Aug 2023, 18:43

Also, the URL for 'weatherOz' package was broken and now fixed

Regards
Peter

## Changelog

* Fixed breaking changes in Roxygen to use _PACKAGE special sentinel
       in 'cropgrowdays-package.R' file to ensure CRAN compliance
* Changed URL reference to 'weatherOz' package, which is now an
       'ropensci' package, in vignette

## R CMD check results

### Development PC: Fedora 39 Linux, R 4.3.3

$ R CMD check --as-cran cropgrowdays_0.2.1.tar.gz
Status: OK

> devtools::check('~/Data/dev/cropgrowdays/cropgrowdays')
0 errors | 0 warnings | 0 notes

Which is essentially the same for 3 Rhub machines and MacOS M1 checks

Also, this package has no reverse dependencies

### Rhub Machines

> rhub::check_for_cran()

#### Platform: 	Windows Server 2022, R-devel, 64 bit
Build ID: 	cropgrowdays_0.2.1.tar.gz-dc995a95feae4fa8a53359a5e455454a
Platform: 	Windows Server 2022, R-devel, 64 bit
Build time: 	4 minutes 4.7 seconds
NOTES:
* checking HTML version of manual ... NOTE
Skipping checking math rendering: package 'V8' unavailable
* checking for non-standard things in the check directory ... NOTE
Found the following files/directories:
  ''NULL''
* checking for detritus in the temp directory ... NOTE
Found the following files/directories:
  'lastMiKTeXException'
* DONE

#### Platform: 	Fedora Linux, R-devel, clang, gfortran
Build ID: 	cropgrowdays_0.2.1.tar.gz-c252b60686a54aa9ba614b0cc9a5f294
PBuild time: 	59 minutes 23 seconds
NOTES:
* checking HTML version of manual ... NOTE
Skipping checking HTML validation: no command 'tidy' found
Skipping checking math rendering: package 'V8' unavailable

#### Platform: 	Ubuntu Linux 20.04.1 LTS, R-release, GCC
Build ID: 	cropgrowdays_0.2.1.tar.gz-5d974d24a8e849298f2e42303afd797a
Platform: 	Ubuntu Linux 20.04.1 LTS, R-release, GCC
Build time: 	1 hour 1 minute 26.6 seconds
NOTES:
* checking HTML version of manual ... NOTE
Skipping checking HTML validation: no command 'tidy' found
Skipping checking math rendering: package 'V8' unavailable
Build time: 	18 minutes 46.6 seconds

### macOS builder results

> devtools::check_mac_release()
* using R version 4.3.0 Patched (2023-05-18 r84451)
* using platform: aarch64-apple-darwin20 (64-bit)
* R was compiled by
    Apple clang version 14.0.0 (clang-1400.0.29.202)
    GNU Fortran (GCC) 12.2.0
* running under: macOS Ventura 13.3.1

No errors, warnings or notes

* DONE
Status: OK
* using check arguments '--no-clean-on-error '
* elapsed time (check, wall clock): 0:32

## revdepcheck results

> revdepcheck::revdep_check(num_workers = 4)
We checked 0 reverse dependencies, comparing R CMD check results across CRAN and dev versions of this package.

 * We saw 0 new problems
 * We failed to check 0 packages
